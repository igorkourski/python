'''
"GBP     1,123"         should not match (too many spaces)
"USD 0.10"              should match
"GBP 1,123,412"         should match
"CAD 10.00"             should match
"CAD 10"                should match
"ABC 10"                should not match (wrong currency)
"qUSD 10"               should not match (redundant characters at the beginning)
"USD 10q"               should not match (redundant characters at the end)
"CAD 10.1231"           should not match (fractional part too long)
"USD 012.12"            should not match (redundant leading zero)
'''
import re

pattern = '^\d*[.,]?\d*$'
temp = ['1000.5 - 2000.55']
#strings = re.findall('^\d*[.,]?\d*$', temp[0])  //  '\d+\.\d+|\d+'  []
strings = re.findall('\d+\.\d+|\d+$', temp[0])
print(strings)

'''
'''

sh = ['GBP     1,123','USD 0.10','GBP 1,123,412','CAD 10,1231']
shopping = ['bread','butter','cheese']

for value in sh:
    value.replace(',','')
    print(value)

print (re.search(r'\bGBP\b', 'GBP     1,123'))
#for value in prices:
#    value.replace(',',`'')
#    print(value)

