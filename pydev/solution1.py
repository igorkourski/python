import builtins
import sys
def dynamic_coin_changing(C, k): 
    n = len(C) 
    # create two-dimensional array with all zeros 
    dp = [[0] * (k + 1) for i in range(n + 1)] 
    dp[0] = [0] + [sys.maxsize] * k 
    for i in range(1, n + 1):
        for j in range(C[i - 1]): 
            dp[i][j] = dp[i - 1][j] 
            for j in range(C[i - 1], k + 1): 
                dp[i][j] = min(dp[i][j - C[i - 1]] + 1, dp[i - 1][j]) 
    return dp[n] 

def dynamic_coin_changing2(C, k): 
    n = len(C)
    dp = [0] + [sys.maxsize] * k
    for i in range(1, n + 1): 
        for j in range(C[i - 1], k + 1): 
            dp[j] = min(dp[j - C[i - 1]] + 1, dp[j]) 
    return dp

def frog(S, k, q):
    n = len(S)
    dp = [1] + [0] * k 
    for j in range(1, k + 1): 
        for i in range(n): 
            if S[i] <= j: 
                dp[j] = (dp[j] + dp[j - S[i]]) % q; 
    return dp[k] 

C = [1,3,5,10,50] 
k = 8

print(dynamic_coin_changing(C, k))
print(dynamic_coin_changing2(C, k))