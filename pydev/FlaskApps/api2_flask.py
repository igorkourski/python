'''
Created on Jan 17, 2019

@author: Igor
'''
from flask import Flask
import time
import requests

app = Flask(__name__)

@app.route('/')


def hello_world():
    start = time.time()
    response = requests.get("http://igorkourski.000webhostapp.com/sandbox/one.php?ig=kr&kr=ig")
    data = response.json()
    end = time.time()
    #response = requests.get("http://igorkourski.000webhostapp.com/sandbox/one.php?ig=kr&kr=ig")
    #return 'response=' + str(data) + "<br/>time=" + str(end-start)
    return str(data)

app.run(debug=True)