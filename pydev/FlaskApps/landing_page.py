'''
Created on Jan 18, 2019

@author: Igor
'''
from flask import Flask
import requests
import time
import json

app = Flask(__name__)

debug = True

def element(string):
    return '<li>' + string + '</li>'

@app.route('/')
def hello_world():

    base = 'http://127.0.0.1:5000' if debug else 'http://igoryk.pythonanywhere.com'

    string = '<br />Hi Igor,welcome to Flask!<br />'
    string = string + "<ul>"
    string = string + element('<a href="' + base + '/public_api">public_api</a>')
    #string = string + '<br /><a href="' + base + '/my_api">my_api</a>'
    string = string + element(base + '/php_api')
    string = string + '</ul>'
    return string

@app.route('/public_api')
def public_api():
    # Get the response from the API endpoint.
    '''
    {"people": [
        {"name": "Oleg Kononenko", "craft": "ISS"},
        {"name": "David Saint-Jacques", "craft": "ISS"},
        {"name": "Anne McClain", "craft": "ISS"}],
     "number": 3,
     "message": "success"
     }'
    '''
    start = time.time()
    response = requests.get("http://api.open-notify.org/astros.json")
    data = response.json()
    end = time.time()

    # num of people are currently in space.
    num = data['number']
    people = ''
    if num >0:
        people = "  names=" + parse_data(data)

    current = '<br />people are currently in space  num=' + str(num) + people;
    #print(num)
    #print(data)
    return 'time=' + str(end-start) + current

def parse_data(data):
    string = ''
    for element in data['people']:
        string = string + (element['name'])
        if element!=data['people'][-1]:
            string = string + ', '

    #if string != '' and len(string)>2 :
    #    string[:-1]
    #    string[:-1]
    return string

def api_parsing(api_url):
    response = requests.get(api_url)

    if response.status_code >= 500:
        print('[!] [{0}] Server Error'.format(response.status_code))
        return None
    elif response.status_code == 404:
        print('[!] [{0}] URL not found: [{1}]'.format(response.status_code,api_url))
        return None
    elif response.status_code == 401:
        print('[!] [{0}] Authentication Failed'.format(response.status_code))
        return None
    elif response.status_code >= 400:
        print('[!] [{0}] Bad Request'.format(response.status_code))
        #print(ssh_key )
        print(response.content )
        return None
    elif response.status_code >= 300:
        print('[!] [{0}] Unexpected redirect.'.format(response.status_code))
        return None
    elif response.status_code == 201:
        added_key = json.loads(response.content)
        return added_key
    elif response.status_code == 200:
        added_key = json.loads(response.content)
        return added_key
    else:
        print('[?] Unexpected Error: [HTTP {0}]: Content: {1}'.format(response.status_code, response.content))
        return None

@app.route('/my_api')
def my_api():

    api_url = 'http://igorkourski.000webhostapp.com/sandbox/one.php?ig=kr&kr=ig'
    api_url = "http://api.open-notify.org/astros.json"

    #data = response.json()
    #ip = data['http_x_forwarded']
    #return "http_x_forwarded=" + str(ip)
    return str(api_parsing(api_url))

if debug : app.run(debug=True)
