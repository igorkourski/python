'''
Created on Nov 1, 2018

@author: Igor
'''
import os

newpath = r'C:\Users\Igor\eclipse-workspace\one\pydev\data' 
if not os.path.exists(newpath):
    os.makedirs(newpath)

filename = 'file.txt'

writepath = os.path.join(newpath, filename)

mode = 'a+' if os.path.exists(writepath) else 'w+'
with open(writepath, mode) as f:
  f.write('Hello, world!\n')
  
  

file = open("testfile.txt","w")
print (file)
file.write("hello world")
file.write("new line")
file.write("another line")
print (file)
file.close()

